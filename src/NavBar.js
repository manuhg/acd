import React from 'react';
import { Link } from "react-router-dom";
var title = "Advocate's Case Diary";

class NavBar extends React.Component {
    constructor()
    {
      super();
      this.active = [true, false, false, false, false];
    }
    set_active(index)
    {
      var i,
        len = this.active.length;
      if (index > len) 
        return;
      for (i = 0; i < len; i++) 
        if (i !== index) 
          this.active[i] = "";
    this.active[index] = "active";
    }
    render()
    {
      this.set_active(this.props.activeIndex);
      return (
        <nav className="navbar navbar-inverse">
          <div className="container-fluid">
            <div className="navbar-header">
              <a
                style={{
                textDecoration: "none",
                color: "black"
              }}
                className="navbar-brand">{title}</a>
            </div>
            <ul className="nav navbar-nav">
              <li className={this.active[0]}>
                <Link to="/">Home</Link>
              </li>
              <li className={this.active[1]}>
                <Link to="/about">about</Link>
              </li>
              <li className={this.active[2]}>
                <Link to="/caselist">caselist</Link>
              </li>
              <li className={this.active[3]}>
                <Link to="/cdetails">cdetails</Link>
              </li>
              <li className={this.active[4]}>
                <Link to="/search">search</Link>
              </li>
            </ul>
          </div>
        </nav>
      );
    }
  }
  export default NavBar;
  