function query_data(url,action, data) {
    console.log("\nQuerying the data"+JSON.stringify({action:action,data:data}));
    return fetch(url, {
            method: 'POST',
            body: JSON.stringify({action:action,data:data})
        }).then(function (response) {
        return response.json();
    });
}
export default query_data;