import Home from './Home';
import About from './About';
import caselist from './caselist';
import cdetails from './cdetails';
import search from './search';
import './App.css';

import React from 'react';
import {BrowserRouter as Router, Route} from "react-router-dom";
//import {BrowserRouter as Router, Route, Link} from "react-router-dom";
//var title = "Advocate's Case Diary";
import logo from './img/advocate.png';

class App extends React.Component {

  render() {
    return (
      <div className="App">
        <Router>
          <div>
            <img className="advimg" src={logo} alt="logo"/><br/>
            <Route exact path="/" component={Home}/>
            <Route path="/about" component={About}/>
            <Route path="/caselist" component={caselist}/>
            <Route path="/cdetails" component={cdetails}/>
            <Route path="/search" component={search}/>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
