import React from 'react';
import './login.css'
import judiciary_logo from './img/judiciary.jpg';
import query_data from './query_data'
class Login extends React.Component {
    constructor() {
        super();
        this.state = {
            loggedin: false, error:false
        };
    }
    error()
    {
        this.setState({loggedin:this.state.loggedin,error:true});
    }
    hide_error()
    {
        this.setState({loggedin:this.state.loggedin,error:false});
    }
    query()
    {
        console.log("kfhaskhdashdjashd");
        var data=query_data('actions.php','login',new FormData(document.getElementById('loginform')));
        console.log(data);
        if(data)
            this.props.rerouter(data);
        else
            this.error();
    }
    render()
    {
        var ds=(this.state.error)?"block":"none";
        if (!this.state.loggedin) 
            return (
                <div id="login">
                    <form id="loginform">
                        <div className="imgcontainer">
                            <img src={judiciary_logo} alt="Avatar" className="avatar"/>
                        </div>

                        <div className="container">
                        <span id="errspan1" style={{color:'red',display:ds}}><h3>errrorrr</h3></span>

                            <label for="uname">
                                <b>Username</b>
                            </label><br/>
                            <input type="text" placeholder="Enter Username" name="uname" required/><br/>

                            <label for="psw">
                                <b>Password</b>
                            </label><br/>
                            <input type="password" placeholder="Enter Password" name="psw" required/><br/>

                            <button type="submit" onClick={()=>this.query()} >Login</button><br/>
                            <label>
                                <input type="checkbox" name="remember"/>
                                Remember me
                            </label>
                            <br/><br/><br/>
                        </div>
                    </form>
                </div>
            );
        else 
            return (
                <div id="login"></div>
            );
        }
    }
export default Login;