import React from 'react';
import NavBar from './NavBar';
import Login from './login';
var title = "Advocate's Case Diary";

class Home extends React.Component {
    constructor()
    {
      super();
      this.state = {
        data: null
      };
    }
    reroute(data)
    {
      this.setState({data: data});
    }
    render()
    {
      if (this.state.data) 
        return (
          <div>
            <NavBar activeIndex={0}/>
            <h2>Welcome to {title}</h2>
            {JSON.stringify(this.state.data)}</div>
        );
      
      return (<Login rerouter={this
        .reroute
        .bind(this)}/>);
    }
  }
export default Home;  